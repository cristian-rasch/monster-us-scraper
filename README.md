# Monster::US::Scraper

monster.com job scraper

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'monster-us-scraper', git: 'git@bitbucket.org:cristian-rasch/monster-us-scraper.git', require: 'monster/us/scraper'
```

And then execute:

    $ bundle

## Usage

```ruby
require "monster/us/scraper"
require "pp"

# Options include:
#   - date_rage - how far back to search for results
#                 (defaults to 14 days, valid values are 0 - a.k.a today, 1, a.k.a yesterday,
#                  3, 7, 14, 30 and nil a.k.a no date range)
#   - job_types - restrict the search to the specified job types
#                 (defaults to temporary and contract jobs)
#   - limit - how many jobs to retrieve (defaults to 20, max. 20)
#   - proxies - a list of http proxies to tunnel web traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
#               defaults to reading them from the MONSTER_PROXIES env var or nil if missing
#   - logger - a ::Logger instance to log error messages to
monster_us = Monster::US::Scraper.new

# Job search
jobs = monster_us.job_search("web developer", date_range: 7, limit: 1)
pp jobs.first
# #<struct Monster::US::Scraper::Job
#  title="Sr. UI/UX Developer",
#  url=
#   "http://jobview.monster.com:80/Sr-UI-UX-Developer-Job-Sacramento-CA-US-144383733.aspx?mescoid=1700179001001&jobPosition=2",
#  id="144383733",
#  employer="Tech-Net Inc",
#  created_on=2015-02-22 10:40:09 UTC,
#  location="Sacramento, CA",
#  salary=nil,
#  category=nil,
#  occupations=nil,
#  industry=nil,
#  education_requirements=nil,
#  experience_requirements="7+ to 10 Years",
#  career_level=nil,
#  description_html=
#   "<p><span style=\"font-family: Cambria, serif;\">Good day..">
```
