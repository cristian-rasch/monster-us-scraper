require_relative "scraper/version"
require "dotenv"
Dotenv.load
require "nokogiri"
require "open-uri"
require "chronic"
require "logger"

module Monster
  CONTRACT_JOB_TYPE = "Contract".freeze
  TEMP_JOB_TYPE = "Temp".freeze
  DEFAULT_JOB_TYPES = [CONTRACT_JOB_TYPE, TEMP_JOB_TYPE].freeze

  module US
    class Scraper
      DEFAULT_LIMIT = 100
      DEFAULT_DATE_RANGE = 14
      MAX_PAGE = 25
      USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0".freeze

      class Job < Struct.new(:title, :url, :id, :employer, :created_on, :location, :salary,
                             :category, :occupations, :industry, :education_requirements,
                             :experience_requirements, :career_level, :description_html)
      end

      # Options include:
      #   - date_rage - how far back to search for results
      #                 (defaults to 14 days, valid values are 0 - a.k.a today, 1, a.k.a yesterday,
      #                  3, 7, 14, 30 and nil a.k.a no date range)
      #   - job_types - restrict the search to the specified job types
      #                 (defaults to temporary and contract jobs)
      #   - limit - how many jobs to retrieve (defaults to 20, max. 20)
      #   - proxies - a list of http proxies to tunnel web traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
      #               defaults to reading them from the MONSTER_PROXIES env var or nil if missing
      #   - logger - a ::Logger instance to log error messages to
      def initialize(options = {})
        @options = options
        configure_logger(options[:logger])
        configure_proxies(options[:proxies])
      end

      def job_types
        @job_types ||= DEFAULT_JOB_TYPES.map(&:downcase)
      end

      # Options include:
      #   - date_rage - how far back to search for results
      #                 (defaults to 14 days, valid values are 0 - a.k.a today, 1, a.k.a yesterday,
      #                  3, 7, 14, 30 and nil a.k.a no date range)
      #   - job_types - restrict the search to the specified job types
      #                 (defaults to temporary and contract jobs)
      #   - limit - how many jobs to retrieve (defaults to 20, max. 20)
      #   - callback - an optional block to be called with each job found ASAP
      def job_search(keywords, options = {})
        limit = (options[:limit] || @options[:limit] || DEFAULT_LIMIT)
        limit = [limit, DEFAULT_LIMIT].min

        max_page = options[:max_page] || @options[:max_page] || MAX_PAGE
        max_page = [max_page, MAX_PAGE].min

        date_range = if options.key?(:date_range)
                       options[:date_range]
                     elsif @options.key?(:date_range)
                       @options[:date_range]
                     else
                       DEFAULT_DATE_RANGE
                     end
        job_search_url = search_url.dup

        job_search_url = customize_search_url(job_search_url, @options.merge(options))

        if date_range
          job_search_url << (job_search_url.include?("?") ? "&" : "?")
          job_search_url << "tm="
          job_search_url << case date_range
                        when 0 then "Today"
                        when 1 then "Yesterday"
                        when 3, 7, 14, 30 then "Last-#{date_range}-Days"
                        end
        end

        job_search_url << (job_search_url.include?("?") ? "&" : "?")
        kws = CGI.escape(Array(keywords).map { |kws| kws.gsub(/\s+/, "-") }.join(","))
        job_search_url << "q=#{kws}"

        current_page = 1
        jobs = []

        begin
          jobs_url = job_search_url + "&pg=#{current_page}"
          io = open_through_proxy(jobs_url)
          break unless io

          jobs_page = Nokogiri::HTML(io)
          job_nodes = jobs_page.css('tr[class*="even"]')  | jobs_page.css('tr[class*="odd"]')
          break if job_nodes.empty?

          job_nodes.each do |job_node|
            title_link = job_node.at_css(".slJobTitle")
            title = title_link.text
            job_url = title_link["href"]
            id = title_link["name"]
            employer = job_node.at_css(".companyContainer a")["title"]
            md = job_node.at_css(".fnt20").text.match(/Posted:\s+/)
            created_on = Chronic.parse(md.post_match.rstrip).utc
            location = job_node.at_css(".jobLocationSingleLine").text[/Location:\s+(.+)/, 1].rstrip

            if io = open_through_proxy(job_url)
              job_page = Nokogiri::HTML(io)

              if employment_type_node = job_page.at_css('[itemprop="employmentType"]')
                employment_type = employment_type_node.text
                if (parent_node = employment_type_node.parent) && parent_node["class"] == "multipledd"
                  employment_type << parent_node.next_sibling.text
                end
                employment_type.gsub!(/\s+/, " ")

                location_node = job_page.at_css('[itemprop="jobLocation"]')
                location = location_node.text.rstrip if location_node

                salary_node = job_page.at_css('[itemprop="baseSalary"]')
                salary = salary_node.text if salary_node

                category, occupations = job_page.css('[itemprop="occupationalCategory"]').map(&:text)

                industry_node = job_page.at_css('[itemprop="industry"]')
                industry = industry_node.text.split(/\s{2,}/).join(", ") if industry_node

                education_requirements_node = job_page.at_css('[itemprop="educationRequirements"]')
                education_requirements = education_requirements_node.text if education_requirements_node

                experience_requirements_node = job_page.at_css('[itemprop="experienceRequirements"]')
                experience_requirements = experience_requirements_node.text if experience_requirements_node

                career_level_node = job_page.at_css('[itemprop="qualifications"]')
                career_level = career_level_node.text if career_level_node

                description_html = job_page.at_css("#TrackingJobBody").inner_html.strip
              end
            end

            job = Job.new(title, job_url, id, employer, created_on, location, salary,
                          category, occupations, industry, education_requirements,
                          experience_requirements, career_level, description_html)
            yield(job) if block_given?
            jobs << job

            break if jobs.size == limit
          end
        end while jobs.size < limit && current_page < max_page

        jobs
      end

      protected

      def customize_search_url(search_url, options)
        jtypes = Array(options[:job_types] || job_types)
        jtypes.map!(&:capitalize)
        jtypes &= DEFAULT_JOB_TYPES
        jtypes = DEFAULT_JOB_TYPES if jtypes.empty?
        search_url << CGI.escape(jtypes.join(" "))
        search_url << "_"
        search_url << param_checksum(job_types: jtypes)
      end

      def param_checksum(options)
        "8" * options[:job_types].size
      end

      def search_url
        search_url ||= "#{base_url}/search/"
      end

      def base_url
        @base_url ||= "http://jobsearch.monster.com".freeze
      end

      private

      def configure_logger(logger = nil)
        @logger = logger || @options[:logger] || Logger.new(STDERR)
      end

      def configure_proxies(proxies)
        @proxies = proxies || @options[:proxies] || ENV.fetch("MONSTER_PROXIES").split(",")
      end

      def open_through_proxy(url)
        begin
          open(url, "User-Agent" => USER_AGENT, "proxy" => next_proxy)
        rescue => err
          @logger.error "#{err.message} opening '#{url}'"
          nil
        end
      end

      def next_proxy
        @proxies.shift.tap { |proxy| @proxies << proxy }
      end
    end
  end
end
