require File.expand_path("test/test_helper")
require File.expand_path("lib/monster/us/scraper")

module Monster
  module US
    class ScraperTest < Minitest::Test
      def setup
        @monster_us = Scraper.new
        @keywords = "web developer"
      end

      def test_date_range_filtering
        jobs = @monster_us.job_search(@keywords, limit: 1, date_range: 3)
        assert_equal 1, jobs.size
      end

      def test_date_job_type_filtering
        jobs = @monster_us.job_search(@keywords, limit: 1, job_types: "contract")
        assert_equal 1, jobs.size
      end

      def test_getting_three_jobs
        jobs = @monster_us.job_search(@keywords, limit: 3)
        assert_equal 3, jobs.size
      end

      def test_max_page_option
        jobs = @monster_us.job_search(@keywords, max_page: 1, limit: 100)
        assert_operator jobs.size, :<=, 50
      end

      def test_passing_in_a_callback_to_job_search
        job = nil
        @monster_us.job_search(@keywords, limit: 1) do |j|
          job = j
        end
        refute_nil job
      end
    end
  end
end
